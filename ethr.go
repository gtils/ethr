package ethr

// Monad :: x -> y
type Monad func(interface{}) interface{}

// Ethr interface
type Ethr interface {
	IsLeft() bool
	IsRight() bool

	RMap(Monad) Ethr
	LMap(Monad) Ethr

	Fold(Monad, Monad) interface{}
}

// New :: c -> Ethr x y
func New(condition bool, l interface{}, r interface{}) Ethr {
	if condition {
		return right{r}
	}
	return left{l}
}

// Left :: x -> Ethr x nil
func Left(value interface{}) Ethr {
	return left{value}
}

// Right :: y -> Ethr nil y
func Right(value interface{}) Ethr {
	return right{value}
}

// FromLeft :: x -> Ethr x y -> x
func FromLeft(defaultValue interface{}, ethr Ethr) interface{} {
	return ethr.Fold(func(leftValue interface{}) interface{} {
		return leftValue
	}, func(_ interface{}) interface{} {
		return defaultValue
	})
}

// FromRight :: y -> Ethr x y -> y
func FromRight(defaultValue interface{}, ethr Ethr) interface{} {
	return ethr.Fold(func(_ interface{}) interface{} {
		return defaultValue
	}, func(rightValue interface{}) interface{} {
		return rightValue
	})
}
