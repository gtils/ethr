package ethr

type right struct {
	value interface{}
}

func (r right) IsLeft() bool                           { return false }
func (r right) IsRight() bool                          { return true }
func (r right) RMap(f Monad) Ethr                      { return right{f(r.value)} }
func (r right) LMap(_ Monad) Ethr                      { return r }
func (r right) Fold(_ Monad, rmonad Monad) interface{} { return rmonad(r.value) }
