package ethr

import (
	"reflect"
	"testing"
)

func Test_left_IsLeft(t *testing.T) {
	tests := []struct {
		name string
		l    left
		want bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.l.IsLeft(); got != tt.want {
				t.Errorf("left.IsLeft() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_left_IsRight(t *testing.T) {
	tests := []struct {
		name string
		l    left
		want bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.l.IsRight(); got != tt.want {
				t.Errorf("left.IsRight() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_left_RMap(t *testing.T) {
	type args struct {
		in0 Monad
	}
	tests := []struct {
		name string
		l    left
		args args
		want Ethr
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.l.RMap(tt.args.in0); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("left.RMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_left_LMap(t *testing.T) {
	type args struct {
		f Monad
	}
	tests := []struct {
		name string
		l    left
		args args
		want Ethr
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.l.LMap(tt.args.f); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("left.LMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_left_Fold(t *testing.T) {
	type args struct {
		lmonad Monad
		in1    Monad
	}
	tests := []struct {
		name string
		l    left
		args args
		want interface{}
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.l.Fold(tt.args.lmonad, tt.args.in1); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("left.Fold() = %v, want %v", got, tt.want)
			}
		})
	}
}
