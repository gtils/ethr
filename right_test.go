package ethr

import (
	"reflect"
	"testing"
)

func Test_right_IsLeft(t *testing.T) {
	tests := []struct {
		name string
		r    right
		want bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.r.IsLeft(); got != tt.want {
				t.Errorf("right.IsLeft() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_right_IsRight(t *testing.T) {
	tests := []struct {
		name string
		r    right
		want bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.r.IsRight(); got != tt.want {
				t.Errorf("right.IsRight() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_right_RMap(t *testing.T) {
	type args struct {
		f Monad
	}
	tests := []struct {
		name string
		r    right
		args args
		want Ethr
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.r.RMap(tt.args.f); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("right.RMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_right_LMap(t *testing.T) {
	type args struct {
		in0 Monad
	}
	tests := []struct {
		name string
		r    right
		args args
		want Ethr
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.r.LMap(tt.args.in0); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("right.LMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_right_Fold(t *testing.T) {
	type args struct {
		in0    Monad
		rmonad Monad
	}
	tests := []struct {
		name string
		r    right
		args args
		want interface{}
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.r.Fold(tt.args.in0, tt.args.rmonad); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("right.Fold() = %v, want %v", got, tt.want)
			}
		})
	}
}
