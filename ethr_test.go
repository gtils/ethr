package ethr

import (
	"reflect"
	"testing"
)

func TestNew(t *testing.T) {
	type args struct {
		condition bool
		l         interface{}
		r         interface{}
	}
	tests := []struct {
		name string
		args args
		want Ethr
	}{
		{"Left ethr", args{false, "LeftValue", "RightValue"}, left{"LeftValue"}},
		{"Right ethr", args{true, "LeftValue", "RightValue"}, right{"RightValue"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := New(tt.args.condition, tt.args.l, tt.args.r); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLeft(t *testing.T) {
	type args struct {
		value interface{}
	}
	tests := []struct {
		name string
		args args
		want Ethr
	}{
		{"Simple left either", args{"LeftValue"}, left{"LeftValue"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Left(tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Left() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRight(t *testing.T) {
	type args struct {
		value interface{}
	}
	tests := []struct {
		name string
		args args
		want Ethr
	}{
		{"Simple right ethr", args{"RightValue"}, right{"RightValue"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Right(tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Right() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFromLeft(t *testing.T) {
	type args struct {
		defaultValue interface{}
		ethr         Ethr
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{"FromLeft to left ethr", args{true, left{false}}, false},
		{"FromLeft to right ethr", args{true, right{false}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromLeft(tt.args.defaultValue, tt.args.ethr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FromLeft() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFromRight(t *testing.T) {
	type args struct {
		defaultValue interface{}
		ethr         Ethr
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{"FromRight to left ethr", args{true, left{false}}, true},
		{"FromRight to right ethr", args{true, right{false}}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromRight(tt.args.defaultValue, tt.args.ethr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FromRight() = %v, want %v", got, tt.want)
			}
		})
	}
}
