package ethr

type left struct {
	value interface{}
}

func (l left) IsLeft() bool                           { return true }
func (l left) IsRight() bool                          { return false }
func (l left) RMap(_ Monad) Ethr                      { return l }
func (l left) LMap(f Monad) Ethr                      { return left{f(l.value)} }
func (l left) Fold(lmonad Monad, _ Monad) interface{} { return lmonad(l.value) }
